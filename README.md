## Sistema de préstamos para el examen de Seguridad de autitoria con Laravel Auditing
#### Actualizado


#### Implementado con Laravel Auditing

#### Uni, Alex Mamani Llojlla
## Año 2022 

CARRERA DE INGENIERIA DE SISTEMAS
UNIVERIDAD PUBLICA DE EL ALTO
MATERIA AUDITORIA DE SISTEMAS

### Usurios
Luego de correr con exito la migracion y los seeders, el sistema crea varios usuarios para comenzar a probar

__Rol__: `admin`
__User__:`admin@admin.com`
__Contraseña__:`12345678`


__Rol__: `supervisor`
__User__:`supervisor@supervisor.com`
__Contraseña__:`12345678`


__Rol__: `agente`
__User__:`agente@agente.com`
__Contraseña__:`12345678`

